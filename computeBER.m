function [ber] = computeBER(data_bit,y,tx_wave,Pars)
    % reduce power
    tx_wave = tx_wave./ 10.^(Pars.tx_pow/20);
    % normalize
    tx_pow = mean(abs(tx_wave).^2);
    rx_pow = mean(abs(y).^2);
    y = y .* sqrt(tx_pow / rx_pow);
    
    y = reshape(y,[Pars.N_fft + Pars.N_cyclepref 1 Pars.N_blocks]);

    dem_sig = ofdm_demod(y,Pars.N_fft,Pars.N_cyclepref,Pars.pilot_pos,Pars.guard_bands);

    sample = qamdemod(dem_sig,Pars.M);
    
%     scatterplot(dem_sig(:,:,1));

    for i = 1: size(sample,3)
        demod_bit(:,:,i) = de2bi(sample(:,:,i),'left-msb');
        [numErr(i),ber(i)]= biterr(data_bit(:,:,i),demod_bit(:,:,i));    
    end
    
    ber = mean(ber);
    
%     dem_sig = ofdm_demod(y,Pars.N_fft,Pars.N_cyclepref,Pars.pilot_pos,Pars.guard_bands);
% 
%     dem_sig = reshape(dem_sig,[numel(dem_sig) 1]);
%     
%     sample = qamdemod(dem_sig,Pars.M);
%     
%     est_bit = de2bi(sample,'left-msb');
% 
%    [ ~,ber ] = biterr(data_bit,est_bit);
    
end

