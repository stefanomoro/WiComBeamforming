close all
clear all 
clc
Pars.fc = 1e9;
Pars.c = physconst('LightSpeed');
Pars.lambda = Pars.c / Pars.fc;
%Geometry Definition
System.BS.pos = [0,0,25];
System.V1.posStart = [70,-100,1.5];
System.V1.posEnd = [70,100,1.5];
System.V2.posStart = [10,-210,1.5];
System.V2.posEnd = [10,100,1.5];
System.I1.posStart = [100,10,1.5];
System.I2.posStart = [-150,100,1.5];

System.V1.DoA = computeDoA(System.BS,System.V1);
System.V2.DoA = computeDoA(System.BS,System.V2);

System.I1.DoA = computeDoA(System.BS,System.I1);
System.I2.DoA = computeDoA(System.BS,System.I2);


createScenarioAndVisualize(System,Pars);

System.BS.array = phased.URA('Size',[4,4],'ElementSpacing',[Pars.lambda/2 Pars.lambda/2],'ArrayNormal','x');
System.BS.antPos = getElementPosition(System.BS.array);

[System.V1.waveform Pars.fs]= generateLTEUplink();
System.V2.waveform = generateLTEUplink();

System.I1.waveform = generateLTEUplink();
System.I2.waveform = generateLTEUplink();




System.BS.receivedW = collectPlaneWave(System.BS.array,...
    [System.V1.waveform System.V2.waveform,System.I1.waveform,System.I2.waveform],[System.V1.DoA' System.V2.DoA' System.I1.DoA' System.I2.DoA'],Pars.fc);

two_r_chan = phased.TwoRayChannel('SampleRate',Pars.fs,'GroundReflectionCoefficient',-1,'OperatingFrequency',Pars.fc,'CombinedRaysOutput',true);
prop_sig = two_r_chan([System.V1.waveform,System.V1.waveform],System.V1.posStart',System.BS.pos',[0,0,0]',[0,0,0]')


rx = System.BS.receivedW;
size(rx)









function createScenarioAndVisualize(Geometry,Pars)
    plot3(Geometry.BS.pos(1),Geometry.BS.pos(2),Geometry.BS.pos(3),'or');
    text(Geometry.BS.pos(1),Geometry.BS.pos(2),Geometry.BS.pos(3)-0.1,'BS');
    hold on
    plot3(Geometry.V1.posStart(1),Geometry.V1.posStart(2),Geometry.V1.posStart(3),'sb');
    text(Geometry.V1.posStart(1),Geometry.V1.posStart(2),Geometry.V1.posStart(3)-0.1,'V1');
    plot3(Geometry.V2.posStart(1),Geometry.V2.posStart(2),Geometry.V2.posStart(3),'sb');
    text(Geometry.V2.posStart(1),Geometry.V2.posStart(2),Geometry.V2.posStart(3)-0.1,'V2');
    
    plot3(Geometry.I1.posStart(1),Geometry.I1.posStart(2),Geometry.I1.posStart(3),'*m');
    text(Geometry.I1.posStart(1),Geometry.I1.posStart(2),Geometry.I1.posStart(3)-0.1,'I1');
    plot3(Geometry.I2.posStart(1),Geometry.I2.posStart(2),Geometry.I2.posStart(3),'*m');
    text(Geometry.I2.posStart(1),Geometry.I2.posStart(2),Geometry.I2.posStart(3)-0.1,'I2');
    hold off
    xlim([-300,300]);
    ylim([-300,300]);
    zlim([0,30]);
    grid on
end

function DoA = computeDoA(A,B)
    AoA = atan2(A.pos(2) - B.posStart(2),A.pos(1) - B.posStart(1));
    ZoA = atan2(A.pos(3) - B.posStart(3),A.pos(1) - B.posStart(1));
    DoA = [AoA ZoA];
end
