function [x,x_bit] = generateNarrowBandSignal(N_bit,M)
%GENERATENARROWBANDSIGNAL Summary of this function goes here
%   Detailed explanation goes here

x_bit = randi([0 1],N_bit,1);
x_bit = reshape(x_bit,ceil(length(x_bit)/log2(M)),log2(M));
x_sample = bi2de(x_bit,'left-msb');


x = qammod(x_sample,M);

end

