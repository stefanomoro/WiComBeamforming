function [rx_demod] = ofdm_demod(rx_sig,N_fft,N_cyclepref,pilot_pos,guard_bands)
%OFDM_DEMOD Summary of this function goes here
%   Detailed explanation goes here
%     ofdm_len =N_fft + N_cyclepref;
%     iters = ceil(length(rx_sig) /ofdm_len);
%     rx_demod = [];
%     for i = 1:iters
%         st_ps = (i-1)*ofdm_len + 1;
%         end_ps = (i*ofdm_len);
%         y = rx_sig(st_ps:end_ps);
%         y = y(N_cyclepref+1:end,:,:);
%         % move to freq
%         rx_data = fft(y,N_fft);
%         % remove guard bands and pilots
%         rx_data(pilot_pos,:,:) = [];
%         rx_demod= [rx_demod rx_data(guard_bands(1)+1 : end - guard_bands(2),:,:)];
%     end
    
    rx_sig = rx_sig(N_cyclepref+1:end,:,:);
    % move to freq
    rx_data = fft(rx_sig,N_fft);
    % remove guard bands and pilots
    rx_data(pilot_pos,:,:) = [];
    rx_demod= rx_data(guard_bands(1)+1 : end - guard_bands(2),:,:);

end

