close all
clear all 
clc

%% Parameters setting
Pars.fc = 3.8e9;
Pars.c = physconst('LightSpeed');
Pars.lambda = Pars.c / Pars.fc;
Pars.N_tx_elem = [4 4];
Pars.N_bit = 1e2;
Pars.M = 4;
Pars.N_vehicles = 2; 
Pars.N_all_mobile = 4;
Pars.scan_az = -90:1:90;
Pars.scan_elev = -90:1:0;
Pars.tx_pow = 20; %dBm
%OFDM
Pars.N_fft = 64;
Pars.N_cyclepref = 16;
Pars.pilot_pos = [12;26;40;54];
Pars.pil = 3+3j;
Pars.guard_bands = [6,5];
Pars.N_bit_block = (Pars.N_fft - max(size(Pars.pilot_pos)) - sum(Pars.guard_bands)) * log2(Pars.M); %block is 1 ofdm symbol
Pars.N_blocks = ceil(Pars.N_bit/Pars.N_bit_block);
Pars.fs = 10e6; %10MHz


%Geometry Definition

System = createScenario3D(Pars);

%% Waveform Generation

% Generate OFDM Signal

%[System.V1.waveform,System.V1.data_bit] = generateOFDMSignal(Pars);
%[System.V2.waveform,System.V2.data_bit] = generateOFDMSignal(Pars);
%[System.I1.waveform ,~]= generateOFDMSignal(Pars);
%[System.I2.waveform ,~]= generateOFDMSignal(Pars);

% Generate LTEUplink
[System.V1.waveform,System.V1.data_bit] = generateLTEUplink(Pars);
[System.V2.waveform,System.V2.data_bit] = generateLTEUplink(Pars);
[System.I1.waveform ,~]= generateLTEUplink(Pars);
[System.I2.waveform ,~]= generateLTEUplink(Pars);

% Amplify signal
System.V1.waveform = System.V1.waveform .* 10.^(Pars.tx_pow/20);
System.V2.waveform = System.V2.waveform .* 10.^(Pars.tx_pow/20);
System.I1.waveform = System.I1.waveform .* 10.^(Pars.tx_pow/20);
System.I2.waveform = System.I2.waveform .* 10.^(Pars.tx_pow/20);


%% Setup Quadriga

l = qd_layout;
l.set_scenario('QuaDRiGa_UD2D_LOS');
l.simpar.center_frequency = Pars.fc;
txArr = qd_arrayant('omni');
rxArr = qd_arrayant('omni');
rxArr.no_elements = prod(Pars.N_tx_elem);
rxArr.element_position = System.BS.antPos;
l.tx_array = txArr;
l.rx_array = rxArr;
l.no_rx = 1;
l.no_tx = Pars.N_all_mobile;
System.V1.qd_track = qd_track('linear',...
        pathLength(System.V1.posStart,System.V1.posEnd),...
        atan((System.V1.posEnd(2)-System.V1.posStart(2))/(System.V1.posEnd(1)-System.V1.posStart(1))));
System.V1.qd_track.name = 'trackV1'; 
System.V2.qd_track = qd_track('linear',...
        pathLength(System.V2.posStart,System.V2.posEnd),...
        atan((System.V2.posEnd(2)-System.V2.posStart(2))/(System.V2.posEnd(1)-System.V2.posStart(1))));
System.V2.qd_track.name = 'trackV2'; 
System.V1.qd_track.initial_position = System.V1.posStart;
System.V2.qd_track.initial_position = System.V2.posStart;

l.tx_position = [System.V1.posStart, System.V2.posStart,System.I1.posStart,System.I2.posStart];
l.tx_track(1,1) = copy(System.V1.qd_track);
l.tx_track(1,2) = copy(System.V2.qd_track);
l.rx_position = System.BS.pos;



l.visualize([],[],0);
l.set_pairing;
chan = l.get_channels;


%% Do convolution for each vechicle and position

track_len = chan(1).no_snap; % number of positions
snr_val= 0:10;
snr_conv = zeros(length(snr_val),Pars.N_vehicles,track_len);
snr_null = zeros(length(snr_val),Pars.N_vehicles,track_len);
snr_mvdr = zeros(length(snr_val),Pars.N_vehicles,track_len);
snr_wiener = zeros(length(snr_val),Pars.N_vehicles,track_len);
snr_lms = zeros(length(snr_val),Pars.N_vehicles,track_len);
ber_conv = zeros(length(snr_val),Pars.N_vehicles,track_len);
ber_null = zeros(length(snr_val),Pars.N_vehicles,track_len);
ber_mvdr = zeros(length(snr_val),Pars.N_vehicles,track_len);
ber_wiener = zeros(length(snr_val),Pars.N_vehicles,track_len);
ber_lms = zeros(length(snr_val),Pars.N_vehicles,track_len);
music_noise = zeros(length(snr_val),Pars.N_vehicles,track_len);
Ts = 1 / Pars.fs;

all_waveforms = [System.I1.waveform,System.I2.waveform,System.V1.waveform,System.V2.waveform];
for i = 1:track_len
    % Do convolution
    N_sample = length(System.I1.waveform);
    all_rx = zeros(N_sample,prod(Pars.N_tx_elem));
    all_sig = zeros(N_sample,prod(Pars.N_tx_elem),Pars.N_all_mobile);
    for vehicle = 1:Pars.N_all_mobile
        chTaps = size(chan(vehicle).delay); % number of taps/ each dimension
        wave = all_waveforms(:,vehicle);
        TsVect = 0:Ts:(Ts*N_sample-Ts);
        for antenna = 1:chTaps(1)
            for path = 1:chTaps(3)
                inX = TsVect - chan(vehicle).delay(antenna,1,path,i);
                inY = interp1(TsVect,wave,inX,'pship');
                all_rx(:,antenna) = transpose(inY * chan(vehicle).coeff(antenna,1,path,i)) +...
                    all_rx(:,antenna); % sum all contributions of the each path
                
                all_sig(:,antenna,vehicle) = transpose(inY * chan(vehicle).coeff(antenna,1,path,i)) +...
                    all_sig(:,antenna,vehicle); % sum all contributions of each path for a specific vehicle
            end
        end
    end
    
    
    
    
    %all_sig = [System.V1.rx_wav(:,i) System.V2.rx_wav(:,i),System.I1.rx_wav,System.I2.rx_wav];
    real_doa = [System.V1.DoA(:,i) System.V2.DoA(:,i) System.I1.DoA System.I2.DoA] ./pi .* 180;
    %all_rx = collectPlaneWave(System.BS.array,all_sig,real_doa,Pars.fc);
    
   
    x = computeReference(all_rx,[System.V1.waveform System.V2.waveform]);
    data_bits(:,:,:,1) = System.V1.data_bit ;
    data_bits(:,:,:,2) = System.V2.data_bit;
    tx_waves = [System.V1.waveform System.V2.waveform];
    
    for j = 1:Pars.N_vehicles % Computation is done for every Vehicle (Excluding interferers)
        for snr_idx = 1:length(snr_val) % Computation is done for a range of SNRs
            tic
            snr_in = snr_val(snr_idx);
            
            all_rx_n = awgn(all_rx,snr_in,10*log10(mean(abs(x(:,j)).^2)));
            
            musicEstimator = phased.MUSICEstimator2D('SensorArray',System.BS.array,...
                'OperatingFrequency',Pars.fc,'AzimuthScanAngles',Pars.scan_az,'ElevationScanAngles',Pars.scan_elev,...
                'DOAOutputPort',true,'NumSignalsSource','Property','NumSignals',Pars.N_all_mobile);
            
            [~,est_doa] = musicEstimator(all_rx_n);
            est_doa = real_doa; % bypass Music Estimation
            if( isnan(est_doa(1,4)))
                "not ok"
                real_doa
                est_doa
                snr_in
                i
                j
                est_doa = real_doa;
            end
            %plotSpectrum(musicEstimator,'NormalizeResponse',true);
            toc
            %return
            tic
            est_doa = sortDoA(est_doa,real_doa);
            %est_doa = real_doa;
            steer_vec = steerVecURA(System.BS.array,Pars.lambda,est_doa);
            music_noise(snr_idx,j,i) = sum(abs(real_doa-est_doa),'all');
            
            
            noise_in = all_rx_n - all_rx;
            
            %% SNR and BER for different beamforming techniques
            % Conventional
            y = convBeamforming(all_rx_n,steer_vec(:,j),Pars.N_tx_elem);
            noise_out = y-x(:,j);
            gain = 10*log10(mean(mean(abs(noise_in).^2))) - 10*log10(mean(abs(noise_out).^2));
            snr_conv(snr_idx,j,i) = snr_in + gain;
            %ber_conv(snr_idx,j,i) = computeBER(data_bits(:,:,:,j),y,tx_waves(:,j),Pars);
            
            % Null
            g_1 = [0 0 0 0];
            g_1(j) = 1;
            y = nullBeamforming(all_rx_n,steer_vec,g_1);
            noise_out = y-x(:,j);
            gain = 10*log10(mean(mean(abs(noise_in).^2))) - 10*log10(mean(abs(noise_out).^2));
            snr_null(snr_idx,j,i) = snr_in + gain;
            %ber_null(snr_idx,j,i) = computeBER(data_bits(:,:,:,j),y,tx_waves(:,j),Pars);
            
            % MVDR
            y = MVDR(all_rx,all_rx_n,all_sig,steer_vec(:,j),steer_vec,Pars.N_tx_elem);
            noise_out = y - x(:,j);
            gain = 10*log10( mean(mean(abs(all_rx_n - all_rx).^2)) / mean(abs(noise_out).^2) );
            snr_mvdr(snr_idx,j,i) = gain + snr_in;
            %ber_mvdr(snr_idx,j,i) = computeBER(data_bits(:,:,:,j),y,tx_waves(:,j),Pars);
            
            % Wiener MMSE
            y = Wiener(all_rx_n,x(:,j));
            noise_out = y - x(:,j);
            gain = 10*log10( mean(mean(abs(all_rx_n - all_rx).^2)) / mean(abs(noise_out).^2));
            snr_wiener(snr_idx,j,i) = gain + snr_in;
            %ber_wiener(snr_idx,j,i) = computeBER(data_bits(:,:,:,j),y,tx_waves(:,j),Pars);
            
            % LMS
            y = LMS(all_rx_n,x(:,j),Pars.N_tx_elem);
            noise_out = y - x(:,j);
            gain = 10*log10( mean(mean(abs(all_rx_n - all_rx).^2)) / mean(abs(noise_out).^2));
            snr_lms(snr_idx,j,i) = gain + snr_in;
            %ber_lms(snr_idx,j,i) = computeBER(data_bits(:,:,:,j),y,tx_waves(:,j),Pars);
            
            
            
            toc
        end
    end
    
end
%% Averages on all positions and all vehicles
snr_conv = mean(mean(snr_conv,3),2);
snr_null = mean(mean(snr_null,3),2);
snr_mvdr = mean(mean(snr_mvdr,3),2);
snr_wiener = mean(mean(snr_wiener,3),2);
snr_lms_v = mean(snr_lms,2);
snr_lms = mean(mean(snr_lms,3),2);

%ber_conv = mean(mean(ber_conv,3),2);
%ber_null = mean(mean(ber_null,3),2);
%ber_mvdr = mean(mean(ber_mvdr,3),2);
%ber_wiener = mean(mean(ber_wiener,3),2);
%ber_lms_v = mean(ber_lms,2);
%ber_lms = mean(mean(ber_lms,3),2);

music_noise = mean(mean(music_noise,3),2);

%% Plotting
% SNR
figure
plot(snr_val,snr_null,'gs-')
hold on
plot(snr_val,snr_conv,'kx--')
plot(snr_val,snr_mvdr,'bo-')
plot(snr_val,snr_wiener,'kd-')
plot(snr_val,snr_lms,'r--')

title("SNR Performance")

legend("Null","Conventional","MVDR","Wiener","LMS");
xlabel("SNR input");
ylabel("SINR output");
hold off;
grid on;

% LMS SNR comparison for the two positions
figure
plot(snr_val,snr_lms_v(:,:,1),'g--')
hold on
plot(snr_val,snr_lms_v(:,:,2),'r--')

title("LMS SNR Performance in the different locations")

legend("StartPos","EndPos");
xlabel("SNR input");
ylabel("SINR output");
hold off;
grid on;

% BER
% figure 
% semilogy(snr_val,ber_null,'gs-')
% hold on
% semilogy(snr_val,ber_conv,'kx--')
% semilogy(snr_val,ber_mvdr,'bo-')
% semilogy(snr_val,ber_wiener,'kd-')
% semilogy(snr_val,ber_lms,'r--')
% title("BER Performance")
% 
% legend("Null","Conventional","MVDR","Wiener","LMS");
% xlabel("SNR input");
% ylabel("BER");
% hold off;
% grid on;
% 
% LMS BER comparison for the two positions
% figure
% semilogy(snr_val,ber_lms_v(:,:,1),'g--')
% hold on
% semilogy(snr_val,ber_lms_v(:,:,2),'r--')
% 
% title("LMS BER Performance in the different locations")
% 
% legend("StartPos","EndPos");
% xlabel("SNR input");
% ylabel("BER");
% hold off;
% grid on;

% Music estimation error
figure
plot(snr_val,music_noise,'kd-')
return


%% Functions

function sortedDoA = sortDoA(est,real)
% The estimated angles are sorted comparing the real ones
    sortedDoA = zeros(size(real));
    for i = 1:size(est,2)
        t = repmat(est(:,i),[1 size(real,2)]);
        [min_val,idx] = min(sum(abs(t-real),1));
        sortedDoA(:,idx) =est(:,i); 
        
    end
end

function DoA = computeDoA(BS,track)
    AoA = atan2(track(2,:) - BS.pos(2),track(1,:) - BS.pos(1));
    
    ZoA = atan2(track(3,:) - BS.pos(3),track(1,:) - BS.pos(1));
    DoA = [AoA ;ZoA];
end


function  dist = pathLength(BS_pos,track)
    dist = sqrt((BS_pos(1)-track(1,:)).^2 + (BS_pos(2)-track(2,:)).^2 + (BS_pos(3)-track(3,:)).^2);
end

function p = midpoint(A)
    p = (A.posStart + A.posEnd)/2;
end

function y = convBeamforming(all_rx,s0,num_tx_el)
    % weighted response for all directions
    y = zeros(size(all_rx,1),1);
    num_tx_el = prod(num_tx_el);
    w = s0 / num_tx_el;
    for i = 1:size(all_rx,1)
        y(i) = w' * transpose(all_rx(i,:));
    end
end

function y = nullBeamforming(all_rx,S,g_1)
    % Null-beamforming
    S_inv = pinv(S);
    w_h = g_1 * S_inv;
    y =  all_rx * transpose(w_h);
end

function y = MVDR(all_rx,all_rx_n,all_sig,s_0,S,N_tx_el)
    %N_tx_el = prod(N_tx_el);
    % Correlation between all signals [4x4]
    %U = transpose(all_sig) * transpose(all_sig)';
    %n_pow = mean(mean(abs(all_rx_n - all_rx).^2));
    
    %Ru = S * U * S' + n_pow .* eye(N_tx_el) /size(all_rx,1); % alternative formulation. Ru == Ru_
    
    Ru = transpose(all_rx_n) * transpose(all_rx_n)'./ (size(all_rx_n,1));
    w_mvdr = inv(Ru) * s_0 / (s_0' * inv(Ru) * s_0);
    %w_mvdr = w_mvdr / sum(abs(w_mvdr));
    y(:,1) = w_mvdr' * transpose(all_rx_n);
end

function y = Wiener(all_rx_n,x_j)
    dn = transpose(x_j); % reference signal
    un = transpose(all_rx_n); % received signal
    N_sample = size(all_rx_n,1);
    p = (un * dn') /N_sample; % cross correlation beteween rx data and reference
    Ru = transpose(all_rx_n) * transpose(all_rx_n)'./ (size(all_rx_n,1));
    w_opt = inv(Ru) * p;
    y(:,1) = w_opt' * un;
end

function y = LMS(all_rx_n,x_j,N_tx_el)
    dn = transpose(x_j); % reference signal
    un = transpose(all_rx_n); % received signal
    N_sample = size(un,1);
    Ru = un * un'./ N_sample;
    mu = 1/trace(Ru);
    wn = rand(prod(N_tx_el),1) + 1i*rand(prod(N_tx_el),1);
    wn = wn ./ sum(abs(wn));
    for iter = 1:100
        en = un' * wn - dn'; % error of actual iteration
        mse_grad = 2*un * en /N_sample; % gradient of the MSE with the actual wn
        wn = wn - 0.5 * mu * mse_grad;
    end
    y(:,1) = wn' * un;
end

function x = computeReference(all_rx,orig_sig)
    x = zeros(size(orig_sig));
    for i = 1:size(orig_sig,2)
        pow_rx = mean(mean(abs(all_rx).^2));
        pow_tx = mean(abs(orig_sig(:,i)).^2);
        x(:,i) = orig_sig(:,i) .* sqrt(pow_rx/pow_tx);
    end
end

