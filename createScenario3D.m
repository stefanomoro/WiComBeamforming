function System = createScenario3D(Pars)
    System.BS.pos = [0,0,40]';
    System.V1.posStart = [70,-100,1.5]';
    System.V1.posEnd = [80,150,1.5]';
    System.V1.track = [System.V1.posStart midpoint(System.V1)  System.V1.posEnd];
    System.V1.distances = pathLength(System.BS.pos,System.V1.track);
    System.V2.posStart = [100,200,1.5]';
    System.V2.posEnd = [200,210,1.5]';
    System.V2.track = [System.V2.posStart midpoint(System.V2)  System.V2.posEnd];
    System.V2.distances = pathLength(System.BS.pos,System.V2.track);
    System.I1.posStart = [100,10,1.5]';
    System.I1.distances = pathLength(System.BS.pos, [System.I1.posStart]);
    System.I2.posStart = [150,100,1.5]';
    System.I2.distances = pathLength(System.BS.pos, [System.I2.posStart]);

    System.V1.DoA = computeDoA(System.BS,System.V1.track);
    System.V2.DoA = computeDoA(System.BS,System.V2.track);

    System.I1.DoA = computeDoA(System.BS,System.I1.posStart);
    System.I2.DoA = computeDoA(System.BS,System.I2.posStart);

    

    System.BS.array = phased.URA( Pars.N_tx_elem, ...
            'ElementSpacing', 0.5*Pars.lambda, ...
            'Element', phased.IsotropicAntennaElement('BackBaffled', false));
    System.BS.antPos = getElementPosition(System.BS.array);


    figure
    plot3(System.BS.pos(1),System.BS.pos(2),System.BS.pos(3),'or');
    text(System.BS.pos(1),System.BS.pos(2),System.BS.pos(3)-0.1,'BS');
    hold on
    plot3(System.V1.posStart(1),System.V1.posStart(2),System.V1.posStart(3),'sb');
    text(System.V1.posStart(1),System.V1.posStart(2),System.V1.posStart(3)-0.1,'V1');
    plot3(System.V2.posStart(1),System.V2.posStart(2),System.V2.posStart(3),'sb');
    text(System.V2.posStart(1),System.V2.posStart(2),System.V2.posStart(3)-0.1,'V2');
    
    plot3(System.I1.posStart(1),System.I1.posStart(2),System.I1.posStart(3),'*m');
    text(System.I1.posStart(1),System.I1.posStart(2),System.I1.posStart(3)-0.1,'I1');
    plot3(System.I2.posStart(1),System.I2.posStart(2),System.I2.posStart(3),'*m');
    text(System.I2.posStart(1),System.I2.posStart(2),System.I2.posStart(3)-0.1,'I2');
    hold off
    xlim([-300,300]);
    ylim([-300,300]);
    zlim([0,30]);
    grid on
    
    function p = midpoint(A)
    p = (A.posStart + A.posEnd)/2;
    end
    function  dist = pathLength(BS_pos,track)
        dist = sqrt((BS_pos(1)-track(1,:)).^2 + (BS_pos(2)-track(2,:)).^2 + (BS_pos(3)-track(3,:)).^2);
    end
    function DoA = computeDoA(BS,track)
        AoA = atan2(track(2,:) - BS.pos(2),track(1,:) - BS.pos(1));

        ZoA = atan2(track(3,:) - BS.pos(3),track(1,:) - BS.pos(1));
        DoA = [AoA ;ZoA];
    end


end

