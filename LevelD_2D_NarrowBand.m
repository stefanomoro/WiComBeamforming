close all
clear all 
clc

%% Parameters setting
Pars.fc = 3.8e9;
Pars.c = physconst('LightSpeed');
Pars.lambda = Pars.c / Pars.fc;
Pars.N_tx_elem = 8;
Pars.N_bit = 1e4;
Pars.M = 4;
Pars.N_vehicles = 2; 
Pars.N_all_mobile = 4;
Pars.scan_az = -90:.01:90;
Pars.tx_pow = 20; %dBm

%% Geometry Definition
System = createScenario2D(Pars);

%% Waveform Generation
% Generate NarrowBand Signal
System.BS.antPos = getElementPosition(System.BS.array);
[System.V1.waveform,System.V1.data_bit ]= generateNarrowBandSignal(Pars.N_bit,Pars.M);
[System.V2.waveform,System.V2.data_bit ] = generateNarrowBandSignal(Pars.N_bit,Pars.M);
System.I1.waveform = generateNarrowBandSignal(Pars.N_bit,Pars.M);
System.I2.waveform = generateNarrowBandSignal(Pars.N_bit,Pars.M);

% Amplify signal
System.V1.waveform = System.V1.waveform .* 10.^(Pars.tx_pow/20);
System.V2.waveform = System.V2.waveform .* 10.^(Pars.tx_pow/20);
System.I1.waveform = System.I1.waveform .* 10.^(Pars.tx_pow/20);
System.I2.waveform = System.I2.waveform .* 10.^(Pars.tx_pow/20);

%% Propagate Signal
System.V1.rx_wav =  10.^(-fspl(System.V1.distances,Pars.lambda)/20) .* System.V1.waveform;
System.V2.rx_wav =  10.^(-fspl(System.V2.distances,Pars.lambda)/20) .* System.V2.waveform;
System.I1.rx_wav =  10.^(-fspl(System.I1.distances,Pars.lambda)/20) .* System.I1.waveform;
System.I2.rx_wav =  10.^(-fspl(System.I2.distances,Pars.lambda)/20) .* System.I2.waveform;

%% Cycle for all positions of track and for each vechicle

track_len = length(System.V1.track);
snr_val= 0:10;
snr_conv = zeros(length(snr_val),Pars.N_vehicles,track_len);
snr_null = zeros(length(snr_val),Pars.N_vehicles,track_len);
snr_mvdr = zeros(length(snr_val),Pars.N_vehicles,track_len);
snr_wiener = zeros(length(snr_val),Pars.N_vehicles,track_len);
ber_conv = zeros(length(snr_val),Pars.N_vehicles,track_len);
ber_null = zeros(length(snr_val),Pars.N_vehicles,track_len);
ber_mvdr = zeros(length(snr_val),Pars.N_vehicles,track_len);
ber_wiener = zeros(length(snr_val),Pars.N_vehicles,track_len);

for i = 1:track_len % Computation is done for every position in the track
    % Collect waves
    all_sig = [System.V1.rx_wav(:,i) System.V2.rx_wav(:,i),System.I1.rx_wav,System.I2.rx_wav];
    real_doa = [System.V1.DoA(:,i) System.V2.DoA(:,i) System.I1.DoA System.I2.DoA] ./pi .* 180;
    all_rx = collectPlaneWave(System.BS.array,all_sig,real_doa,Pars.fc);
    
    x = [System.V1.rx_wav(:,i) System.V2.rx_wav(:,i)];
    data_bits(:,:,1) = System.V1.data_bit ;
    data_bits(:,:,2) = System.V2.data_bit;
    tx_waves = [System.V1.waveform System.V2.waveform];
    
    for j = 1:Pars.N_vehicles % Computation is done for every Vehicle (Excluding interferers)
        for snr_idx = 1:length(snr_val) % Computation is done for a range of SNRs
            snr_in = snr_val(snr_idx);
            all_rx_n = awgn(all_rx,snr_in,10*log10(mean(abs(x(:,j)).^2)));
            
            musicEstimator = phased.MUSICEstimator('SensorArray',System.BS.array,...
                'OperatingFrequency',Pars.fc,'ScanAngles',Pars.scan_az,...
                'DOAOutputPort',true,'NumSignalsSource','Property','NumSignals',Pars.N_all_mobile);
            
            [~,est_doa] = musicEstimator(all_rx_n);
            %plotSpectrum(musicEstimator,'NormalizeResponse',true);
            
            est_doa = sortDoA(est_doa,real_doa);
            %est_doa = real_doa;
            steer_vec = steerVecULA(System.BS.array,Pars.lambda,est_doa);
            
            noise_in = all_rx_n - all_rx;
            
            %% SNR and BER for different beamforming techniques
            % Conventional
            y = convBeamforming(all_rx_n,steer_vec(:,j),Pars.N_tx_elem);
            noise_out = y-x(:,j);
            gain = 10*log10(mean(mean(abs(noise_in).^2))) - 10*log10(mean(abs(noise_out).^2));
            snr_conv(snr_idx,j,i) = snr_in + gain;
            
            
            ber_conv(snr_idx,j,i) = BER(data_bits(:,:,j),y,tx_waves(:,j),Pars);
            % Null
            g_1 = [0 0 0 0];
            g_1(j) = 1;
            y = nullBeamforming(all_rx_n,steer_vec,g_1);
            noise_out = y-x(:,j);
            gain = 10*log10(mean(mean(abs(noise_in).^2))) - 10*log10(mean(abs(noise_out).^2));
            snr_null(snr_idx,j,i) = snr_in + gain;
            ber_null(snr_idx,j,i) = BER(data_bits(:,:,j),y,tx_waves(:,j),Pars);
            
            % MVDR
            y = MVDR(all_rx,all_rx_n,all_sig,steer_vec(:,j),steer_vec,Pars.N_tx_elem);
            noise_out = y - x(:,j);
            gain = 10*log10( mean(mean(abs(all_rx_n - all_rx).^2)) / mean(abs(noise_out).^2) );
            snr_mvdr(snr_idx,j,i) = gain + snr_in;
            ber_mvdr(snr_idx,j,i) = BER(data_bits(:,:,j),y,tx_waves(:,j),Pars);
            
            % Wiener MMSE
            y = Wiener(all_rx_n,x(:,j));
            noise_out = y - x(:,j);
            gain = 10*log10( mean(mean(abs(all_rx_n - all_rx).^2)) / mean(abs(noise_out).^2));
            snr_wiener(snr_idx,j,i) = gain + snr_in;
            ber_wiener(snr_idx,j,i) = BER(data_bits(:,:,j),y,tx_waves(:,j),Pars);
            
            
            
        end
    end
    
end

%% Averages on all positions and all vehicles
snr_conv = mean(mean(snr_conv,3),2);
snr_null = mean(mean(snr_null,3),2);
snr_mvdr = mean(mean(snr_mvdr,3),2);
snr_wiener = mean(mean(snr_wiener,3),2);
ber_conv = mean(mean(ber_conv,3),2);
ber_null = mean(mean(ber_null,3),2);
ber_mvdr = mean(mean(ber_mvdr,3),2);
ber_wiener = mean(mean(ber_wiener,3),2);
%% Plotting
% SNR
figure
plot(snr_val,snr_null,'gs-')
hold on
plot(snr_val,snr_conv,'kx--')
plot(snr_val,snr_mvdr,'bo-')
plot(snr_val,snr_wiener,'kd-')

title("SNR Performance")

legend("Null","Conventional","MVDR","Wiener");
xlabel("SNR input");
ylabel("SINR output");
hold off;

% BER
figure 
semilogy(snr_val,ber_null,'gs-')
hold on
semilogy(snr_val,ber_conv,'kx--')
semilogy(snr_val,ber_mvdr,'bo-')
semilogy(snr_val,ber_wiener,'kd-')
title("BER Performance")

legend("Null","Conventional","MVDR","Wiener");
xlabel("SNR input");
ylabel("BER");
hold off;
grid on
return

%% Functions

function sortedDoA = sortDoA(est,real)
% The estimated angles are sorted comparing the real ones
    real = real(1,:);
    sortedDoA = zeros(1,length(real));
    for i = 1:length(est)
        t = repmat(est(i),[1,length(real)]);
        [min_val,idx] = min(abs(t-real));
        sortedDoA(idx) =est(i); 
        
    end
end

function y = convBeamforming(all_rx,s0,num_tx_el)
    % weighted response for all directions
    y = zeros(size(all_rx,1),1);
    
    w = s0 / num_tx_el;
    for i = 1:size(all_rx,1)
        y(i) = w' * transpose(all_rx(i,:));

    end
end

function y = nullBeamforming(all_rx,S,g_1)
    % Null-beamforming
    S_inv = pinv(S);
    w_h = g_1 * S_inv;
    y =  all_rx * transpose(w_h);
end

function y = MVDR(all_rx,all_rx_n,all_sig,s_0,S,N_tx_el)
    % Correlation between all signals [4x4]
    U = transpose(all_sig) * transpose(all_sig)';
    n_pow = mean(mean(abs(all_rx_n - all_rx).^2));
    
    % Both Ru furmulation work fine
    Ru = S * U * S' + n_pow .* eye(N_tx_el) /size(all_rx,1); % alternative formulation. Ru == Ru_
    %Ru = transpose(all_rx_n) * transpose(all_rx_n)'./ (size(all_rx_n,1));
    
    w_mvdr = inv(Ru) * s_0 / (s_0' * inv(Ru) * s_0);
    y(:,1) = w_mvdr' * transpose(all_rx_n);
end

function y = Wiener(all_rx_n,x_j)
    dn = transpose(x_j); % reference signal
    un = transpose(all_rx_n); % received signal
    N_sample = size(all_rx_n,1);
    p = (un * dn') /N_sample; % cross correlation beteween rx data and reference
    Ru = transpose(all_rx_n) * transpose(all_rx_n)'./ (size(all_rx_n,1));
    w_opt = inv(Ru) * p;
    y(:,1) = w_opt' * un;
end

function ber = BER(data_bit,y,tx_wave,Pars)
    % reduce power
    tx_wave = tx_wave./ 10.^(Pars.tx_pow/20);
    % normalize
    tx_pow = mean(abs(tx_wave).^2);
    rx_pow = mean(abs(y).^2);
    y = y .* sqrt(tx_pow / rx_pow);
    
    dem_sig = y;
    
    sample = qamdemod(dem_sig,Pars.M);
    
    est_bit = de2bi(sample,'left-msb');

   [ ~,ber ] = biterr(data_bit,est_bit);
end
