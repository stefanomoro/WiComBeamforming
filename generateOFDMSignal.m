function [cp_sig,data_bit]= generateOFDMSignal(Pars)
%GENERATEOFDMSIGNAL Summary of this function goes here
%   Detailed explanation goes here

data_bit = randi([0 1],Pars.N_bit,1);
data_sample = zeros(Pars.N_bit_block/log2(Pars.M),1,Pars.N_blocks);
% zero padding
if mod(Pars.N_bit,Pars.N_bit_block) > 0
    data_bit(end+1 :  Pars.N_blocks * Pars.N_bit_block, 1) = 0;
end
% prepare data for qam
data_bit = reshape(data_bit,Pars.N_bit_block/log2(Pars.M),log2(Pars.M),Pars.N_blocks);

for i = 1 : size(data_bit,3)
    data_sample(:,:,i) = bi2de(data_bit(:,:,i),'left-msb');
end

data_qam = qammod(data_sample,Pars.M);
% OFDM
cp_sig = ofdm_mod(data_qam,Pars.pilot_pos,Pars.pil,Pars.guard_bands,Pars.N_cyclepref,Pars.N_fft,Pars.N_blocks);
cp_sig = reshape(cp_sig,[numel(cp_sig) 1]);
%data_bit = reshape(data_bit,numel(data_bit)/log2(Pars.M), log2(Pars.M));
end

